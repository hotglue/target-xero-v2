"""Xero target class."""

from typing import List, Optional, Union

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_xero_v2.sinks import XeroSink


class TargetXero(Target):
    """Sample target for Xero."""

    name = "target-xero-v2"

    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(
            config=config,
            parse_env_config=parse_env_config,
            validate_config=validate_config,
        )
        # Process one stream at once.
        self._max_parallelism = 1

    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("access_token", th.StringType, required=True),
        th.Property("tenant_id", th.StringType, required=True),
    ).to_dict()
    default_sink_class = XeroSink


if __name__ == "__main__":
    TargetXero.cli()
