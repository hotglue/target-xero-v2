"""Xero target sink class, which handles writing streams."""
import json
from datetime import datetime
from typing import Dict, List, Optional

from singer_sdk.plugin_base import PluginBase
from singer_sdk.sinks import BatchSink

from target_xero_v2.client import XeroClient
from target_xero_v2.mapping import UnifiedMapping
import singer

LOGGER = singer.get_logger()


class XeroSink(BatchSink):
    """Xero target sink class."""

    max_size = 1000

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)
        # Save config for refresh_token saving
        self.client = None
        self.config_file = target.config_file
        self.all_items = []
        self.acc_list = []
        self.account_codes = []
        self.cat_list = []
        self.tax_list = []
        self.aus_uk_nz = bool(self.config.get("aus_nz_uk"))
        # Default set to true for Tessaract test it more easyily.

    def get_account_status(self, record):
        client = self.get_client()
        # Check if the invoice exists
        try:
            invoice = client.filter(
                "Invoices", invoice_number=record.get("invoiceNumber")
            )
            # Check if it has been approved
            if invoice[0].get("Status") not in ["AUTHORISED", "PAID"]:
                # If status is not 'AUTHORISED' or 'PAID'
                # Return the record to be updated
                return record
            # Else return an empty record
            return None
        except:
            # If the GET fails just pass the record and it will create a new Invoice
            return record

    def get_tax_list(self):
        if not self.tax_list:
            client = self.get_client()
            self.tax_list = {
                i["Name"]: i["TaxType"] for i in client.filter("Tax_Rates")
            }
        return self.tax_list

    def get_accounts_list(self):
        if not self.acc_list:
            client = self.get_client()
            self.acc_list = client.filter("Accounts")
        return self.acc_list

    def get_account_code(self, account_name):
        accounts = self.get_accounts_list()
        # Make sure we don't do lookup on string account code
        account_name = account_name.lower()
        if account_name.isdigit():
            return account_name
        if not self.account_codes:
            codes = {}
            for account in accounts:
                if "Code" in account:
                    codes.update({account["Name"].lower(): account["Code"]})
            self.account_codes = codes
            del codes
        if account_name in self.account_codes:
            return self.account_codes[account_name]
        else:
            return None

    def get_tracking_categories_list(self):
        if not self.cat_list:
            client = self.get_client()
            self.cat_list = client.filter("Tracking_Categories")
        return self.cat_list

    def prepare_accounts_categories(self):
        acc_list = self.get_accounts_list()
        cat_list = self.get_tracking_categories_list()

        # Process accounts
        accounts = {}
        for account in acc_list:
            if account.get("Code") is None:
                continue

            name = account["Name"]
            code = account["Code"]
            acc_ref = {"Name": name, "Code": code}
            accounts[code] = acc_ref
            accounts[name] = acc_ref

        # Process categories
        categories = {}
        for category in cat_list:
            name = category["Name"]
            options = [x["Name"] for x in category["Options"]]

            for option in options:
                categories[option] = {"Name": name, "Option": option}
        return accounts, categories

    def build_lines(self, lines):
        return_lines = []
        accounts, categories = self.prepare_accounts_categories()

        for row in lines:
            posting_type = row["postingType"]
            line_amt = abs(row["amount"])
            if posting_type.lower() == "credit":
                line_amt = -1 * line_amt

            line_item = {"Description": row["description"], "LineAmount": line_amt}

            acct_num = str(row["accountNumber"])
            acct_name = row["accountName"]
            acct_code = accounts.get(acct_num, accounts.get(acct_name, {})).get("Code")

            if acct_code is not None:
                line_item["AccountCode"] = acct_code
            else:
                raise Exception(
                    f"Account is missing on Journal Entry! Name={acct_name} No={acct_num}"
                )

            if row.get("customerName"):
                tracking = categories.get(row.get("customerName"))
                line_item["Tracking"] = [tracking]

            return_lines.append(line_item)
        return return_lines

    def process_journalentries(self, record):
        date = record["transactionDate"]
        date = datetime.strptime(date, "%m/%d/%Y")
        date = date.strftime("%Y-%m-%d")

        # Create the entry
        entry = {
            "Narration": record["id"],
            "Date": date,
            "Status": "POSTED",
        }
        lines = {}
        lines["JournalLines"] = self.build_lines(record["lines"])

        entry.update(lines)
        return entry

    def process_taxrates(self, record):
        # Create the tax_rate
        entry = {
            "Name": record["name"][:50],
            "TaxComponents": [
                {"Name": record["name"][:50], "Rate": str(record["rate"])}
            ],
        }

        if self.aus_uk_nz:
            # "ReportTaxType": record["transType"][:50], # TODO : Allow only with AUS, UK, NZ.
            entry.update({"ReportTaxType": record["transType"][:50]})

        return entry

    def prepare_payload(self, record, stream_name):
        mapping = UnifiedMapping()
        payload = mapping.prepare_payload(record, stream_name, target="xero")

        if stream_name == "contacts":
            for list_field in ["addresses", "phones"]:
                if isinstance(payload.get(list_field), dict):
                    payload.pop(list_field)
        elif stream_name == "items":
            for list_field in ["PurchaseDetails", "SalesDetails"]:
                if not payload.get(list_field):
                    payload.pop(list_field)
        elif stream_name == "invoices":
            # Do bills need this check to?
            if "Contact" in payload:
                if "customerId" not in payload["Contact"]:
                    client = self.get_client()
                    # invoices = client.filter("Invoices",IDs='INV-ID')
                    contact_detail = client.filter(
                        "Contacts",
                        where='Name=="{}"'.format(payload["Contact"]["Name"]),
                    )
                    if contact_detail:
                        contact_detail = contact_detail[0]
                        payload["Contact"]["ContactID"] = contact_detail["ContactID"]
                    else:
                        print(
                            f"Warning: Contact {payload['Contact']['Name']} not found. Skipping."
                        )
                        return None
                        # raise Exception("EXCEPTION: CONTACT NOT FOUND IN XERO")
                # do the items lookup
                # for i,item in enumerate(payload['LineItems']):
                #     account_detail = client.filter(
                #         "Accounts",
                #         where='Name=="{}"'.format("AccountCode"),
                #     )
                #     payload['LineItems'][i]['AccountCode'] = account_detail[0]['Code']

                payload["LineItems"] = self.prepare_invoice_lineitems(payload)

        elif stream_name == "credit_notes":
            client = self.get_client()
            # invoices = client.filter("Invoices",IDs='INV-ID')
            contact_detail = client.filter(
                "Contacts",
                where='Name=="{}"'.format(payload["customerName"]),
            )
            for i, item in enumerate(payload["LineItems"]):
                account_detail = client.filter(
                    "Accounts",
                    where='Name=="{}"'.format(item["AccountCode"]),
                )
                payload["LineItems"][i]["AccountCode"] = account_detail[0]["Code"]

            if contact_detail:
                contact_detail = contact_detail[0]
                if not payload.get("Contact"):
                    payload["Contact"] = {}
                payload["Contact"]["ContactID"] = contact_detail["ContactID"]

            if payload.get("Date"):
                payload["Date"] = payload["Date"].split("T")[0]

            payload["LineAmountTypes"] = "Exclusive"
            payload["Type"] = "ACCPAYCREDIT"

        elif stream_name == "quotes":
            # Do bills need this check to?
            if "Contact" in payload:
                if "customerId" not in payload["Contact"]:
                    client = self.get_client()
                    # invoices = client.filter("Invoices",IDs='INV-ID')
                    contact_detail = client.filter(
                        "Contacts",
                        where='Name=="{}"'.format(payload["Contact"]["Name"]),
                    )
                    if contact_detail:
                        contact_detail = contact_detail[0]
                        payload["Contact"]["ContactID"] = contact_detail["ContactID"]
                    else:
                        print(
                            f"Warning: Contact {payload['Contact']['Name']} not found for quotes. Skipping."
                        )
                        return None    

        return payload

    def prepare_invoice_lineitems(self, payload):
        lineItems = payload["LineItems"]
        items = []
        # allItems = self.get_all_items()
        self.tax_list = None
        taxes = self.get_tax_list()
        for lineItem in lineItems:
            itemName = lineItem.get("ItemName")
            if itemName:
                item = self.get_item(itemName)
                lineItem.pop("ItemName", None)
                if item:
                    lineItem["Item"] = {
                        "ItemID": item["ItemID"],
                        "Name": item["Name"],
                        "Code": item["Code"],
                    }
                    lineItem["ItemCode"] = item["Code"]
            if lineItem.get("Description") is None:
                lineItem["Description"] = "Created via API"
            tax_type = lineItem.get("TaxType")
            if taxes is not None:
                if tax_type in taxes.keys() and tax_type is not None:
                    lineItem["TaxType"] = taxes[tax_type]
            if "AccountCode" in lineItem:
                # check if we need to lookup account code
                if isinstance(lineItem["AccountCode"], str):
                    lineItem["AccountCode"] = self.get_account_code(
                        lineItem["AccountCode"]
                    )
            items.append(lineItem)
        return items

    def get_item(self, itemName):
        return_item = {}
        for item in self.all_items:
            if item["Name"] == itemName:
                return_item = item
                break
        return return_item

    def get_all_items(self):
        client = self.get_client()
        self.all_items = client.filter("Items")

    def process_record(self, record: dict, context: dict) -> None:
        if not context.get("records"):
            context["records"] = []

        if self.stream_name == "JournalEntries":
            entry = self.process_journalentries(record)
            context["records"].append(entry)

        if self.stream_name == "TaxRates":
            taxes = self.get_tax_list()
            entry = self.process_taxrates(record)

            # Check if taxName is already in Xero
            # if it's not, then create it
            if taxes is not None:
                if not entry["Name"] in taxes.keys():
                    context["records"].append(entry)

        if self.stream_name in ["Customers", "Contacts"]:
            contact = self.prepare_payload(record, "contacts")
            contact["IsCustomer"] = True
            # self.logger.info(json.dumps(contact))
            context["records"].append(contact)

        if self.stream_name == "Invoices":
            record = self.get_account_status(record)
            if record:
                invoice = self.prepare_payload(record, "invoices")
                if invoice is not None:
                    invoice["Type"] = self.config.get("invoice_type", "ACCREC")
                    context["records"].append(invoice)

        if self.stream_name == "Bills":
            record = self.get_account_status(record)
            if record:
                invoice = self.prepare_payload(record, "bills")
                if invoice is not None:
                    invoice["Type"] = "ACCPAY"
                    context["records"].append(invoice)

        if self.stream_name == "Items":
            item = self.prepare_payload(record, "items")
            client = self.get_client()
            # res = client.filter("Accounts")
            context["records"].append(item)

        if self.stream_name == "CreditNotes":
            item = self.prepare_payload(record, "credit_notes")
            client = self.get_client()
            # res = client.filter("Accounts")
            context["records"].append(item)
        if self.stream_name == "Quotes":
            item = self.prepare_payload(record, "quotes")
            context["records"].append(item)

    def process_batch(self, context: dict) -> None:
        # Get the journals to post
        records = context.get("records")
        client = self.get_client()

        if self.stream_name == "JournalEntries":
            endpoint = "Manual_Journals"
        if self.stream_name in ["Customers", "Contacts"]:
            endpoint = "Contacts"
        if self.stream_name == "TaxRates":
            endpoint = "TaxRates"
        if self.stream_name in ["Invoices", "Bills"]:
            endpoint = "Invoices"
        if self.stream_name == "Items":
            endpoint = "Items"
        if self.stream_name == "CreditNotes":
            endpoint = "CreditNotes"
        if self.stream_name == "Quotes":
            endpoint = "Quotes"
        # client.check_platform_access(dict(self.config), self.config_file)
        if self.stream_name in ["Customers", "Contacts"]:
            rec = {endpoint: records}
            print(f"Processing {self.stream_name}\n")
            res = client.push(endpoint, rec)
            if res.status_code == 400:
                res = res.text
                raise Exception(res)
            else:
                # res = res.json()
                LOGGER.info(f"DEBUG PAYLOAD {res.text}")
        else:
            for stream in records:
                print(f"Processing {self.stream_name}\n")
                res = client.push(endpoint, stream)
                if res.status_code == 400:
                    res = res.text
                    raise Exception(res)
                else:
                    # res = res.json()
                    LOGGER.info(f"DEBUG PAYLOAD {res.text}")

    def get_client(self):
        if self.client is None:
            self.client = XeroClient(dict(self.config), self.config_file)
            # Refresh the credentials if the access_token is invalid
            self.client.refresh_credentials()
        return self.client
